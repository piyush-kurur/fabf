%
% This is the LaTeX template file for lecture notes for CS267,
% Applications of Parallel Computing.  When preparing 
% LaTeX notes for this class, please use this template.
%
% To familiarize yourself with this template, the body contains
% some examples of its use.  Look them over.  Then you can
% run LaTeX on this file.  After you have LaTeXed this file then
% you can look over the result either by printing it out with
% dvips or using xdvi.
%

\documentclass[twoside]{article}
\setlength{\oddsidemargin}{0.25 in}
\setlength{\evensidemargin}{-0.25 in}
\setlength{\topmargin}{-0.6 in}
\setlength{\textwidth}{6.5 in}
\setlength{\textheight}{8.5 in}
\setlength{\headsep}{0.75 in}
\setlength{\parindent}{0 in}
\setlength{\parskip}{0.1 in}

%
% ADD PACKAGES here:
%

\usepackage{amsmath,amsfonts,graphicx}

%
% The following commands set up the lecnum (lecture number)
% counter and make various numbering schemes work relative
% to the lecture number.
%
\newcounter{lecnum}
\renewcommand{\thepage}{\thelecnum-\arabic{page}}
\renewcommand{\thesection}{\thelecnum.\arabic{section}}
\renewcommand{\theequation}{\thelecnum.\arabic{equation}}
\renewcommand{\thefigure}{\thelecnum.\arabic{figure}}
\renewcommand{\thetable}{\thelecnum.\arabic{table}}

%
% The following macro is used to generate the header.
%
\newcommand{\lecture}[4]{
   \pagestyle{myheadings}
   \thispagestyle{plain}
   \newpage
   \setcounter{lecnum}{#1}
   \setcounter{page}{1}
   \noindent
   \begin{center}
   \framebox{
      \vbox{\vspace{2mm}
    \hbox to 6.28in { {\hfill \bf Fall 2015: Analysis of Boolean
        Functions \hfill} }
       \vspace{4mm}
       \hbox to 6.28in { {\Large \hfill Lecture #1: #2  \hfill} }
       \vspace{2mm}
       \hbox to 6.28in { {\it Lecturer: #3 \hfill Scribes: #4} }
      \vspace{2mm}}
   }
   \end{center}
   \markboth{Lecture #1: #2}{Lecture #1: #2}

   {\bf Note}: {\it LaTeX template courtesy of UC Berkeley EECS dept
     and IITD.}

   {\bf Disclaimer}: {\it Errors and Omissions Expected.}
   \vspace*{4mm}
}

% Use these for theorems, lemmas, proofs, etc.
\newtheorem{theorem}{Theorem}[lecnum]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{observation}[theorem]{Observation}
\newenvironment{proof}{{\bf Proof:}}{\hfill\rule{2mm}{2mm}}

% **** IF YOU WANT TO DEFINE ADDITIONAL MACROS FOR YOURSELF, PUT THEM
% HERE:

\newcommand\E{\mathbb{E}}

\begin{document}
\lecture{1}{August 21}{Amit Sinhababu}{Satyadev Nandakumar}


\section{Introduction}
We recall the usual definition of a Boolean function.

\begin{definition}
Let $n$ be a positive integer. A function $f: \{0,1\}^n \to \{0,1\}$
is called a \emph{boolean function}.
\end{definition}

In this course, however, in addition to the above, we will also
consider boolean functions $f: \{-1,1\}^n \to \{-1,1\}$, $f:\{0,1\}
\to \mathbb{R}$ and $f:\{-1,1\}^n \to \mathbb{R}$ as boolean
functions.

The most common functions in this course will be ones mapping strings
in $\{-1,1\}^n$ to $\{-1,1\}$. Though this might seem a strange
choice, we will justify it during the course in two ways - first, we
will see how this definition naturally arises when we consider Fourier
analysis on general abelian groups, and second, we will observe how
the statements of the results are more elegant in this model.

Now, we consider a few common settings in which Boolean functions
arise. 

\begin{enumerate}
\item In Set Theory: Consider an arbitrary subset $S$ of $\{1,
  2, \dots, n\}$. Then such a set can be represented by an $n$-bit
  string where the $i^\text{th}$ bit is 1 if and only if $i \in S$. 

 A \emph{property} $P$ of sets is a predicate which evaluates to true
 or to false for a given set. Such a property $P$ can be represented
 by a Boolean function $f: \{0,1\}^n \to \{0,1\}$ where the input
 argument is the representation of the set, and the output is 1 if and
 only if $P(S)$ is true.
\item Consider an undirected graph $G=(V,E)$ with vertices $v_1,v_2,
  \dots, v_n$. Assume that there is a standard enumeration of pairs of
  vertices. 

  Then such a graph can be represented by a bit vector of length $n
  \choose 2$ where the $i^\text{th}$ bit is 1 if and only if the
  $i^\text{th}$ pair of vertices has an edge connecting them.

  A property of the graph then has a natural representation as a
  Boolean function
  $f : \{0,1\}^{n \choose 2} \to \{0,1\}$.
\item Boolean Circuits composed of and, or and not gates can be
  represented as Boolean functions of the inputs.
\item Voting Schemes: Suppose there are voters $v_1, v_2, \dots,
  v_n$, who vote for one of two choices. Then a ballot can be
  represented as a bit vector of length $n$ where the $i^\text{th}$
  bit represents the vote of $v_i$. 

  A \emph{voting scheme} is a method to determine a winner, given a
  ballot. Examples of such schemes are the ``dictator'' scheme, which
  says that the vote of some particular voter completely determines
  the outcome, the ``democratic'' scheme, where the outcome is that
  which has the most number of votes, etc. Clearly, a voting scheme
  can be represented as a Boolean function $f: \{0,1\}^n \to
  \{0,1\}$. 
\item Learning Theory : TBD
\end{enumerate}

The course is on the application of Fourier analysis to the study of
Boolean functions. Though Fourier analysis is most commonly used to
study functions on the reals (mapping to the complex numbers), there
is a theory which studies functions defined on a finite Abelian Group
(mapping, again, to the complex numbers). It is the second which will
be used throughout.

For the Fourier analysis of Boolean functions, we must first fix a
suitable representation for a boolean function. There are several
natural candidates for the representation a Boolean function $f:
\{0,1\}^n \to \{0,1\}$.
\begin{enumerate}
\item $f$ can be represented as a truth table,
\item $f$ can be represented as a bit vector of length $2^n$, where
  the $i^\text{th}$ bit is 1 if and only if, on the $i^\text{th}$
  vector in the ascending order of $2^n$-bit strings, $f$ evaluates to
  1.
\item $f$ can be represented as a polynomial. 

In this scheme, every boolean positive literal $x_i$ is represented by
itself, \emph{i.e.} $p_{x_i} = x_i$, every negated literal $\neg x_i$
by $(1-x_i)$, \emph{i.e.} $p_{\neg x_i} = (1-x_i)$ the representation
of $C_1 \land C_2$ will be the product of their representations,
\emph{i.e.} $p_{C_1 \land C_2} = p_{C_1} p_{C_2}$ and the disjunction
of clauses is represented as $p_{C_1 \lor C_2} = p_{C_1} + p_{C_2} -
p_{C_1 C_2}$. This polynomial scheme is designed to satisfy the
crucial property that $p_f= 1$ if $f = 1$ and $p_f = 0$ when $f = 0$.



For example, assume that we are given a Boolean function as a sum of
minterms:
$$f(x_1,x_2, x_3) = (x_1 \land x_2 \land \neg x_3) \lor (\neg x_1
  \land x_2 \land x_3).$$ 
Then this can be represented as a polynomial over \{0,1\}-valued
variables $x_1, x_2, x_3$ as
$$p_f(x_1, x_2, x_3) = M_1 + M_2 - M_1 M_2,$$
where
$$M_1 = x_1 x_2 (1-x_3) \qquad\text{and}\qquad M_2 = (1-x_1)x_2x_3.$$
Observing that $x_i(1-x_i) = 0$ and $x_i^2 = x_i$ for any $i$, we see
that $M_1 M_2 = 0$. Thus the polynomial is
$$p_f(x) = x_1 x_2 (1-x_3) + (1-x_1)x_2x_3.$$ 
\end{enumerate}

The third representation above works for functions over the domain
$\{0,1\}^n$. For this course, we will adopt the polynomial
representation of Boolean functions. However, we now have to determine
the representation of a function over $\{-1,1\}^n$ to $\{-1,1\}$. 

We will represent a string (equivalently, minterm) $a_1 a_2 \dots a_n$
where each $a_i \in \{-1,1\}$ by its characteristic polynomial
$$\hat{f}_{a_1 a_2 \dots a_n} (x_1, x_2, \dots, x_n) =
\left(\frac{1+a_1x_1}{2}\right)\left(\frac{1+a_2x_2}{2}\right) \dots
\left(\frac{1+a_nx_n}{2}\right).$$

We can verify that $p_{a_1\dots a_n}(b_1,b_2, \dots, b_n) = 1$ if for
all $i$, $a_i = b_i$, and $p_{a_1\dots a_n}(b_1,b_2, \dots, b_n) = 0$
if at least one $a_i \ne b_i$. \footnote{The polynomial evaluates to 0
  or 1, not to 1 or -1.}

(To see a possible derivation of the above formula, see the Appendix.)


Every Boolean function over $\{-1,1\}^n$ can now be represented by
converting its ``sum of minterms'' representation directly into a sum
of the polynomial representations of the minterms as follows.

First, we introduce a notation. For any $S \subseteq \{1, \dots, n\}$ and define
$$x^S = \prod_{i \in S} x_i = \chi(S).$$ 
These are the monomials in our representation, also known as
\emph{parity functions} --- they evaluate to 1 if an even number of
variables are -1, and -1 otherwise.\footnote{Another way to justify
  this is via the group homomorphism $h:(\{0,1\}, \oplus) \to
  (\{-1,1\}, \times)$ where $h(0) = 1$ and $h(1) = -1$. Then the
  parity operation in the additive group becomes the monomial above.}

\begin{definition}
Let $f: \{-1,1\}^n \to \{-1,1\}$ be a Boolean function. Then
\begin{align}
\label{eqn:fourier_spectrum}
f(x_1,x_2, \dots, x_n) = \sum_{S \subseteq [n]} \hat{f}_S x^S.
\end{align}
is known as the \emph{Fourier Spectrum} of $f$, and $\hat{f}_S$ are
known as the \emph{Fourier Coefficients}
\end{definition}

\begin{observation}
The polynomial representation of a Boolean function
ove $\{0,1\}^n$ is not a straightforward translation of a ``sum of
minterms''representation into a ``sum of products'' - mainly since the
polynomial representation of disjunctions is not a sum of its
constituent clauses. However, for Boolean functions over $\{-1,1\}^n$,
its poynomial representation is a translation of ``sum of minterms''
into a ``sum of products'' representation. 

Also, negated literals do not have a representation different from
positive literals.

So this representation is more elegant.
\end{observation}

\subsection{Uniqueness of the Fourier Spectrum of a function}

TODO: Prove that we can define a vector space, and the space satisfies
all the vector space properties.

Idea:
The dimension of the space of Boolean functions is $2^n$. There are
$2^n$ minterms, hence equally many parity functions. Also, by
(\ref{eqn:fourier_spectrum}), every Boolean function is a linear
combination of parity functions. Hence the parity functions form a
basis for the space of Boolean functions over $\{-1,1\}^n$.

\subsection{Orthogonality of the Fourier Basis}
To investigate the notiion of orthogonality of the basis of parity
functions, we must introduce an inner product.

\begin{definition}
The \emph{inner product} of two functions $f,g : \{-1,1\}^n \to
\{-1,1\}$ is defined as
$$\langle f, g \rangle = \frac{1}{2^n} \sum_{(x_1, \dots, x_n) \in
  \{-1,1\}^n} f(x_1, \dots, x_n) g(x_1 \dots x_n).$$
\end{definition}

The above definition has a probabilistic interpretation. Consider the
experiment of drawing a vector $\bar{x}$ uniformly drawn at random
from $\{-1,1\}^n$. Then the above expression represents the expected
value of $f(\bar{x}) g(\bar{x})$, the coordinatewise product of
$f(\bar{x})$ and $g(\bar{x})$. \emph{i.e.},
\begin{eqnarray}
\label{eqn:prob_inner_product}
\langle f, g \rangle = E_{x \in_R U\{-1,1\}^n} \left[ f(\bar{x})
    g(\bar{x}) \right].
\end{eqnarray}

\subsection{Plancherel's and Parseval's Identities}

TODO:

\end{document}




% LocalWords:  boolean abelian th undirected Amit Sinhababu Satyadev Nandakumar
% LocalWords:  TBD minterms
